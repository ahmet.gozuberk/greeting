FROM openjdk:8-jre-alpine3.7


ARG APP_NAME="/tmp/workspace/greeting-service/target/welcome-service-0.0.1-SNAPSHOT"
ARG APP_VERSION=""
ARG ARTIFACT_URL=""
ARG BRANCH_NAME=""

ENV APP_PATH="${APP_NAME}.jar" \
    SERVER_PORT=8080 \
    JVM_OPTS="-server -d64"

ADD target/welcome-service-0.0.1-SNAPSHOT.jar /tmp/workspace/greeting-service/target/welcome-service-0.0.1-SNAPSHOT.jar

EXPOSE $SERVER_PORT

COPY wrapper.sh /wrapper.sh

RUN chmod 555 /wrapper.sh

ENTRYPOINT ["/wrapper.sh"] 
